var app = {};

app.RateCollection = Backbone.Collection.extend({
    url: "http://rating.smartjs.academy/rating?hardMode",
    comparator: function (model) {
        return -model.get("points");
    },
    parse: function (response) {
        return response.records;
    },
});

app.IndexView = Backbone.View.extend({
    el: "#list",
    template: _.template($("#list_template").html()),
    initialize: function () {
        var _this = this;

        this.dataFetched = false;
        this.updatesCache = [];
        this.startVersion = 0;

        var currentVersion = this.currentVersion = new Backbone.Model();

        this.updatesCollection = new Backbone.Collection();

        this.currentVersion.on("change", function(){
            _this.renderVersion();
        });

        this.collection.on("sync", function (collection, data) {
            _this.startVersion = data.version;
            _this.setVersion(data.version);
            _this.render();
        });

        this.collection.on("change", function () {
            this.sort();
            _this.render();
        });

        this.updatesCollection.on("add", function (collection, data) {
                var updates = this;
                    usedStack = [];
                updates.each(function(model){
                    var version =  currentVersion.get("version");
                    if(model.attributes.fromVersion == version){
                        _this.collection.set(model.attributes.updates, {
                            remove: false
                        });
                        _this.setVersion(model.attributes.toVersion);
                        _this.hightLightUpdates(model.attributes.updates);
                        usedStack.push(model);
                    }
                });
               
                updates.remove(usedStack);
                
        });

        this.ws();

    },
    render: function () {
        var _this = this;
         _this.$el.empty();
        _this.collection.each(function (model) {
            _this.$el.append(_this.template(model.attributes));
        });

    },
    renderVersion: function(){
        var time = (new Date(this.currentVersion.get("time"))).toLocaleTimeString()
        $("#version").html("Current version: <span class='version' > " + this.currentVersion.get("version") + "</span> on " +  time + " / " + this.startVersion)
          .find('.version').addClass('blink');
        
    },
    renderError: function(msg){
        this.$el.empty().append("<li class='error-msg'>"+msg+"</li>");
    },
    hightLightUpdates: function(updates){
        var _this = this;
        if(updates){
            updates.map(function(update){
                _this.$el.find("#member-"+ update.id)
                                    .addClass("updated");
            });
        }
    },
    ws: function () {
        var _this = this;
        var url = 'ws://rating.smartjs.academy/rating';
        var ws = _this.webSocket = new WebSocket(url);

        ws.onmessage = function (e) {
            var data = JSON.parse(e.data);
            if (data.status && data.status == "CONNECTED") {
                _this.wsConnected();
            } else {
                _this.wsOnmessage(data);
            }
        };
        ws.onclose = function (e) {
            console.info("WebSocket was closed, reconnection... ", new Date());
            _this.wsClose();
            _this.ws();
        }
        ws.onerror = function (e) { _this.wsError(e); }
    },
    setVersion: function(version){
        this.currentVersion.set({version: version , time: (new Date()).getTime()});
    },
    wsOnmessage: function (data) {
            this.updatesCollection.add(data);
    },
    wsConnected: function () {
        var _this = this;
       
        this.collection.fetch({
            error: function(){
                _this.renderError("data was not retrieved");
            }
    });
    },
    wsError: function(e){
        console.log("ws.error", arguments);
        this.renderError("connection error, try later...");
        return (function(ws){
            if(typeof this.attempt == "undefined"){
                this.attempt = 1;
            } else {
                this.attempt++;
            };
            console.log(this.attempt);
            if(this.attempt == 3){
                ws.onclose = function(){};
            }
        })(e.target)
    },
    wsClose: function(e){
        this.collection.reset(null, {silent: true});
        this.updatesCollection.reset(null, {silent: true});
        this.setVersion(0);
        this.renderError("connection was closed wait for reconnection");
    }
})

var indexApp = new app.IndexView({
    collection: new app.RateCollection
})
